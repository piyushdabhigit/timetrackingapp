const { Sequelize, DataTypes } = require('sequelize');
const db = require('../config/db.config'); // Import your database connection configuration

module.exports = (sequelize, DataTypes) => {
  const Timetrackings = sequelize.define('timetrackings', {
    empId: {
      type: DataTypes.INTEGER,
    },
    date: {
      type: DataTypes.DATE,
    },
    timeIn: {
      type: DataTypes.TIME,
    },
    timeOut: {
      type: DataTypes.TIME,
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    }
  });

  return Timetrackings;
};
