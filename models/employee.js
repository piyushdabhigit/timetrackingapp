var { Sequelize, DataTypes } = require('sequelize');



module.exports = (sequelize, Sequelize) => {
    const Employee = sequelize.define("employee", {
        srno: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true 
        },
        name: {
            type: Sequelize.STRING
        },
        email: {
            type: Sequelize.STRING
        },
        gender: {
            type: Sequelize.STRING
        }
    });


    // timetrackings.associate = (models) => {
    //     timetrackings.belongsTo(models.Employee, {
    //         foreignKey: 'empId',
    //         as: 'Employee',
    //     });
    // };
    
    return Employee;
}; 