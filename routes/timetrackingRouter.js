const express = require('express');
const { body, validationResult } = require('express-validator');
const router = express.Router();

// Import your models and Sequelize
const db = require('../models/index');
const { Sequelize } = require('sequelize');

const timetrackings = db.timetrackings;

router.post(
  '/addTime',
  [
    body('empId').isInt().withMessage('Invalid employee ID'),
    body('date').notEmpty().withMessage('Date is required'),
    body('timeIn').notEmpty().withMessage('Time In is required'),
    body('timeOut').notEmpty().withMessage('Time Out is required'),
  ],
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }

      console.log('hey');

      const { empId, date, timeIn, timeOut } = req.body;

      // Create a JavaScript object with correctly formatted time
      const timetrackingData = {
        empId,
        date,
        timeIn: '08:00:00',  // Replace with the actual time in HH:MM:SS format
        timeOut: '17:00:00',  // Replace with the actual time in HH:MM:SS format
      };

      console.log(timetrackingData)
      const timetrackingCreate = await timetrackings.create(timetrackingData);

      res.status(201).json(timetrackingCreate);
      console.log('done');
    } catch (error) {
      console.error('Error creating:', error);
      res.status(500).json({ error: 'Failed to create' });
    }
  }
);

router.put('/updateTime/:id', async (req, res) => {
  try {
    console.log('updateTime');
    
    const updateTT = await timetrackings.update(
      {
        timeIn: req.body.timeIn,
        timeOut: req.body.timeOut
      },
      { where: { id: req.params.id } }
    );
    res.status(201).json(updateTT);
  } catch (error) {
    console.error('Error updating:', error);
    res.status(500).json({ error: 'Failed to update TT' });
  }
});

router.post('/getUserTime', async (req, res) => {
  try {
    console.log('get');
    
    const empId = req.body.empId;
    const date = req.body.date;

    let where = { empId: empId };
    
    if (date) where.date = date;

    const TimetrackingData = await timetrackings.findAll(
      {
        where,
        attributes: [
          [Sequelize.fn('sum', Sequelize.col('timeOut') - Sequelize.col('timeIn')), 'totalHours']
        ],
        group: ['date'] 
      });
      
    let totalTime = '00:00';

    if (TimetrackingData.length) {
      totalTime = TimetrackingData.reduce((sum, tt) => {
        return sum + tt.dataValues.totalHours;
      }, '00:00');
    }

    res.status(201).json({
      empId,
      totalTime,
      timetrackings: TimetrackingData
    });
  } catch (error) {
    console.error('Error retrieve:', error);
    res.status(500).json({ error: 'Failed to get time data' });
  }
});

module.exports = router;
