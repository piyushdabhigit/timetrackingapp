const express = require("express");
const app = express();
const db = require('./models/index.js'); // Make sure your models are in the 'models' directory
const employeeRouter = require('./routes/employee.js');
const timetrackingRouter = require('./routes/timetrackingRouter.js');

// Middleware and route setup
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/timetracking', timetrackingRouter);
app.use('/employee', employeeRouter);

db.sequelize.sync()
  .then(() => {
    console.log("Synced db.");
  })
  .catch((err) => {
    console.log("Failed to sync db: " + err.message);
  });

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}....`);
});
